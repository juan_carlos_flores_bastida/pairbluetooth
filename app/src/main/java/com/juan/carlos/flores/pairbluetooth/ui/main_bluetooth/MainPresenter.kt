package com.juan.carlos.flores.pairbluetooth.ui.main_bluetooth

class MainPresenter(override var view: MainContract.View): MainContract.Presenter {

    override fun start() {
        view.checkCompatibility()
        view.prepareViews()
    }

    override fun handleStartScan() {
        view.startScan()
    }
}