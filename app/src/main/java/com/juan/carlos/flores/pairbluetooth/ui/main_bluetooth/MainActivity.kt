package com.juan.carlos.flores.pairbluetooth.ui.main_bluetooth

import android.Manifest
import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothDevice
import android.content.BroadcastReceiver
import android.content.Intent
import android.content.IntentFilter
import android.content.pm.PackageManager
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.ListView
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.AppCompatButton
import androidx.appcompat.widget.AppCompatTextView
import androidx.appcompat.widget.Toolbar
import androidx.core.app.ActivityCompat
import androidx.core.content.ContextCompat
import com.juan.carlos.flores.pairbluetooth.BroadcastReceiverBluetoothState
import com.juan.carlos.flores.pairbluetooth.R
import com.juan.carlos.flores.pairbluetooth.ScanBluetoothLowEnergy
import com.juan.carlos.flores.pairbluetooth.Utils
import com.juan.carlos.flores.pairbluetooth.data.DeviceBluetooth

class MainActivity : AppCompatActivity(), MainContract.View, View.OnClickListener{

    val TAG: String = MainActivity::class.java.simpleName
    val LOCATION_PERMISSION_CODE = 100

    private var btnSearchDevices: AppCompatButton? = null
    private var labelFoundDevice: AppCompatTextView? = null
    private var listDevices: ListView? = null

    var arrayListDeviceBluetooths: ArrayList<DeviceBluetooth>? = null
    var adapter: AdapterBluetoothDevices? = null
    var BDHashMap: HashMap<String, DeviceBluetooth>? = null

    var BDStateUpdateReciver: BroadcastReceiver? = null
    var bDScanner: ScanBluetoothLowEnergy? = null

    override val presenter: MainContract.Presenter by lazy {
        MainPresenter(this)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        presenter.start()
    }

    override fun onStart() {
        super.onStart()

        registerReceiver(BDStateUpdateReciver, IntentFilter(BluetoothAdapter.ACTION_STATE_CHANGED))
    }

    override fun onStop() {
        super.onStop()
        unregisterReceiver(BDStateUpdateReciver)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_ENABLE_BT) {
            if (resultCode == Activity.RESULT_OK) {

            } else if (resultCode == Activity.RESULT_CANCELED) {
                Utils.toast(this, this.getString(R.string.turn_bluetooth))
            }
        }
    }

    companion object {
        const val REQUEST_ENABLE_BT = 1
        const val SCAN_PERIOD: Long = 15000 //15 seconds
        const val SIGNAL_STRENGTH: Int = -75
    }

    override fun prepareViews() {
        println("PrepareView")
        val toolbar = findViewById<Toolbar>(R.id.toolbar)
        setSupportActionBar(toolbar)
        supportActionBar?.title = "Bluetooth"

        btnSearchDevices = findViewById(R.id.btn_search_devices)
        labelFoundDevice = findViewById(R.id.label_found_devices)
        listDevices = findViewById(R.id.bluetooth_list)

        BDStateUpdateReciver = BroadcastReceiverBluetoothState(this)

        BDHashMap = hashMapOf()
        arrayListDeviceBluetooths = arrayListOf()
        bDScanner = ScanBluetoothLowEnergy(this, SCAN_PERIOD, SIGNAL_STRENGTH)

        adapter = AdapterBluetoothDevices(this, arrayListDeviceBluetooths!!)

        listDevices?.adapter = adapter

        btnSearchDevices?.setOnClickListener(this)
    }

    override fun checkCompatibility() {
        println("checkCompatibility")
        if (!packageManager.hasSystemFeature(PackageManager.FEATURE_BLUETOOTH_LE)) {
            Utils.toast(this, this.getString(R.string.bluetooth_no_supported))
            finish()
        } else {
            Log.d(TAG, "Device supported")
        }
    }

    override fun startScan() {
        btnSearchDevices?.text = this.getString(R.string.searching_devices)

        arrayListDeviceBluetooths?.clear()
        BDHashMap?.clear()

        adapter?.notifyDataSetChanged()

        bDScanner?.start()
    }

    override fun stopScan() {
        btnSearchDevices?.text = this.getString(R.string.scan_again)

        bDScanner?.stop()
    }

    override fun addDevice(device: BluetoothDevice, rssi: Int) {
        println("dispositivo encontrado: $device")
        var address = device.address

        if (!BDHashMap?.contains(address)!!) {
            var bDevice = DeviceBluetooth(device, rssi)
            BDHashMap?.put(address, bDevice)
            arrayListDeviceBluetooths?.add(bDevice)
        } else {
            BDHashMap?.get(address)?.rssi = rssi
        }

        adapter?.notifyDataSetChanged()
    }

    private fun checkPermission(permission: String, requestCode: Int) {
        if (ContextCompat.checkSelfPermission(this, permission) ==
                PackageManager.PERMISSION_DENIED) {
                    ActivityCompat.requestPermissions(this,
                    arrayOf(permission),
                    requestCode)
        } else {
            Utils.toast(this, this.getString(R.string.permission_granted))
            if (!bDScanner?.isScanning()!! || bDScanner?.isScanning()!! == null) {
                presenter.handleStartScan()
            }
        }
    }

    override fun onClick(view: View?) {
        when(view?.id) {
            btnSearchDevices?.id -> {
                println("Click en botón buscar")

                checkPermission(Manifest.permission.ACCESS_COARSE_LOCATION, LOCATION_PERMISSION_CODE)
            }
        }
    }

    override fun onRequestPermissionsResult(
        requestCode: Int,
        permissions: Array<out String>,
        grantResults: IntArray
    ) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults)

        if (requestCode == LOCATION_PERMISSION_CODE) {
            if (grantResults.isNotEmpty()
                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                Utils.toast(this,
                    this.getString(R.string.permission_granted))

                if (!bDScanner?.isScanning()!! || bDScanner?.isScanning()!! == null) {
                    presenter.handleStartScan()
                }
            }
            else {
                Utils.toast(this,
                    this.getString(R.string.permission_denied))
            }
        }
    }
}