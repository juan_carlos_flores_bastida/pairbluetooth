package com.juan.carlos.flores.pairbluetooth.data

import android.bluetooth.BluetoothDevice

data class DeviceBluetooth(
    val device: BluetoothDevice,
    var rssi: Int)