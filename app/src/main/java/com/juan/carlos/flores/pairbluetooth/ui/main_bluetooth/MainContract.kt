package com.juan.carlos.flores.pairbluetooth.ui.main_bluetooth

import android.bluetooth.BluetoothDevice
import com.juan.carlos.flores.pairbluetooth.common.IPresenter
import com.juan.carlos.flores.pairbluetooth.common.IView

interface MainContract {

    interface View: IView<Presenter> {
        fun prepareViews()
        fun checkCompatibility()
        fun startScan()
        fun stopScan()
        fun addDevice(device: BluetoothDevice, rssi: Int)
    }

    interface Presenter: IPresenter {
        override var view: View

        fun handleStartScan()

    }
}