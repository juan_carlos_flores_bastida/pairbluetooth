package com.juan.carlos.flores.pairbluetooth.ui.main_bluetooth

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.BaseAdapter
import androidx.appcompat.widget.AppCompatTextView
import com.juan.carlos.flores.pairbluetooth.R
import com.juan.carlos.flores.pairbluetooth.data.DeviceBluetooth

class AdapterBluetoothDevices(
    context: Context,
    deviceBluetoothList: ArrayList<DeviceBluetooth>
): BaseAdapter() {

    private var context: Context? = null
    private var deviceBluetoothList: ArrayList<DeviceBluetooth>? = null

    init {
        this.context = context
        this.deviceBluetoothList = deviceBluetoothList
    }

    override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
        var viewHolder: ViewHolder?
        var view: View? = null

        if (view == null) {
            view =  LayoutInflater.from(context).inflate(R.layout.item_bluetooth_device, null)
            viewHolder =
                ViewHolder(
                    view
                )
            view.tag = viewHolder
        } else {
            viewHolder = view.tag as? ViewHolder
        }

        val item = getItem(position) as DeviceBluetooth

        //Asignación de valores a elementos gráficos
        //Si el dispositivo no cuenta con nombre se asigna No name
        viewHolder?.name?.text = if (item.device.name.isNullOrEmpty()) "No name" else item.device.name
        viewHolder?.address?.text = context?.getString(R.string.mac_direction, item.device.address)
        viewHolder?.rssi?.text =  context?.getString(R.string.rssi, item.rssi.toString())

        return view!!
    }

    override fun getItem(position: Int): Any {
        return this.deviceBluetoothList?.get(position)!!
    }

    override fun getItemId(position: Int): Long {
        return position.toLong()
    }

    override fun getCount(): Int {
        return this.deviceBluetoothList?.count()!!
    }

    private class ViewHolder(view: View) {
        var name: AppCompatTextView? = null
        var address: AppCompatTextView? = null
        var rssi: AppCompatTextView? = null

        init {
            name = view.findViewById(R.id.name_device)
            address = view.findViewById(R.id.mac_address_device)
            rssi = view.findViewById(R.id.rssi_device)
        }
    }
}