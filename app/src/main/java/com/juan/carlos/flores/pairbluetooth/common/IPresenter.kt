package com.juan.carlos.flores.pairbluetooth.common

interface IPresenter {

    val view : IView<IPresenter>

    fun start()

}