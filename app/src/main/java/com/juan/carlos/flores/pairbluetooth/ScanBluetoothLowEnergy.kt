package com.juan.carlos.flores.pairbluetooth

import android.bluetooth.BluetoothAdapter
import android.bluetooth.BluetoothManager
import android.content.Context
import android.os.Handler
import com.juan.carlos.flores.pairbluetooth.ui.main_bluetooth.MainActivity

class ScanBluetoothLowEnergy(
    activity: MainActivity,
    scanPeriod: Long,
    signalStrength: Int
) {

    private var activity: MainActivity? = null

    private var bluetoothAdapter: BluetoothAdapter? = null

    private var scanning: Boolean? = false

    private var handler: Handler? = null

    private var scanPeriod: Long? = null

    private var signalStrength: Int? = null

    init {
        this.activity = activity
        this.scanPeriod = scanPeriod
        this.signalStrength = signalStrength

        handler = Handler()

        val bluetoothManager =
            activity.getSystemService(Context.BLUETOOTH_SERVICE) as BluetoothManager
        bluetoothAdapter = bluetoothManager.adapter
    }

    fun isScanning(): Boolean {
        return scanning!!
    }

    fun start() {
        if (!Utils.checkBluetooth(bluetoothAdapter)) {
            Utils.requestUserBluetooth(activity!!)
            activity?.stopScan()
        } else {
            scanLeDevice(true)
        }
    }

    fun stop() {
        scanLeDevice(false)
    }

    private fun scanLeDevice(enable: Boolean) {
        if (enable && !scanning!!) {
            Utils.toast(activity?.applicationContext, activity?.getString(R.string.start_scan))

            handler?.postDelayed(Runnable {
                run {
                    Utils.toast(activity?.applicationContext, activity?.getString(R.string.stop_scan))

                    scanning = false
                    bluetoothAdapter?.stopLeScan(scanCallback)

                    activity?.stopScan()
                }
            }, scanPeriod!!)

            scanning = true

            bluetoothAdapter?.startLeScan(scanCallback)
        } else {
            scanning = false
            bluetoothAdapter?.stopLeScan(scanCallback)
        }
    }

    private var scanCallback: BluetoothAdapter.LeScanCallback =
        BluetoothAdapter.LeScanCallback { bluetoothDevice, rssi, bytes ->
            val newRssi = rssi
            if (rssi > signalStrength) {
                handler?.post(Runnable {
                    run {
                        activity.addDevice(bluetoothDevice, newRssi)
                    }
                })
            }
        }
}