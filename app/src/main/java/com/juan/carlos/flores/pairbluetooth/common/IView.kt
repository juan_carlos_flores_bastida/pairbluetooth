package com.juan.carlos.flores.pairbluetooth.common

interface IView<out T: IPresenter> {

    val presenter: T

}