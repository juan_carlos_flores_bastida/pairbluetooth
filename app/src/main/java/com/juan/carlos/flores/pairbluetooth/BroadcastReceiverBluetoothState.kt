package com.juan.carlos.flores.pairbluetooth

import android.bluetooth.BluetoothAdapter
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent

class BroadcastReceiverBluetoothState(context: Context) : BroadcastReceiver() {

    var context: Context? = null

    init {
        this.context = context
    }

    override fun onReceive(context: Context?, intent: Intent?) {
        val action: String = intent?.action!!

        if (action == BluetoothAdapter.ACTION_STATE_CHANGED) {
            val state: Int = intent?.getIntExtra(
                BluetoothAdapter.EXTRA_STATE,
                BluetoothAdapter.ERROR
            )

            when (state) {
                BluetoothAdapter.STATE_OFF -> Utils.toast(
                    context,
                    context?.resources?.getString(R.string.bluetooth_off)
                )
                BluetoothAdapter.STATE_TURNING_OFF -> Utils.toast(
                    context,
                    context?.resources?.getString(R.string.bluetooth_turning_off)
                )
                BluetoothAdapter.STATE_ON -> Utils.toast(
                    context,
                    context?.resources?.getString(R.string.bluetooth_on)
                )
                BluetoothAdapter.STATE_TURNING_ON -> Utils.toast(
                    context,
                    context?.resources?.getString(R.string.bluetooth_turning_on)
                )
            }
        }
    }
}