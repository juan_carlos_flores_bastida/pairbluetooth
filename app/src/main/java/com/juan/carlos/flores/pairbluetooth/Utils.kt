package com.juan.carlos.flores.pairbluetooth

import android.app.Activity
import android.bluetooth.BluetoothAdapter
import android.content.Context
import android.content.Intent
import android.view.Gravity
import android.widget.Toast
import com.juan.carlos.flores.pairbluetooth.ui.main_bluetooth.MainActivity


class Utils {

    companion object {
        fun checkBluetooth(bluetoothAdapter: BluetoothAdapter?): Boolean {

            // Asegura que este habilitado y disponible el bluetooth. Si no,
            // Muestra un dialogo que solicita al usuario habilitar el bluetooth.
            return !(bluetoothAdapter == null || !bluetoothAdapter.isEnabled)
        }

        fun requestUserBluetooth(activity: Activity) {
            val enableBtIntent = Intent(BluetoothAdapter.ACTION_REQUEST_ENABLE)
            activity.startActivityForResult(enableBtIntent, MainActivity.REQUEST_ENABLE_BT)
        }

        fun toast(context: Context?, string: String?) {
            val toast = Toast.makeText(context, string, Toast.LENGTH_SHORT)
            toast.setGravity(Gravity.CENTER or Gravity.BOTTOM, 0, 0)
            toast.show()
        }
    }
}